﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IGRAAAAA
{
    public partial class Form1 : Form
    {
        public class Picture
        {
          public Bitmap Img {get; set;}
          public int Id {get; set;}
        }
        public int answer;
        public int method;
        Pictures pictures = new Pictures { };
        public void new_gane()
        {
            answer = 0;
            pictureBox4.AllowDrop = false;
            pictureBox5.AllowDrop = false;
            pictureBox6.AllowDrop = false;
            pictureBox7.AllowDrop = false;
            pictureBox8.AllowDrop = false;
            pictureBox9.AllowDrop = false;
            pictureBox10.AllowDrop = false;
            pictureBox11.AllowDrop = false;
            switch (method)
            {
                case 0:
                    pictureBox4.Image = pictures.picture2;
                    pictureBox5.Image = null;
                    pictureBox5.AllowDrop = true;
                    pictureBox6.Image = pictures.picture1;
                    pictureBox7.Image = pictures.picture3;
                    pictureBox8.Image = pictures.picture1;
                    pictureBox9.Image = pictures.picture2;
                    pictureBox10.Image = pictures.picture2;
                    pictureBox11.Image = pictures.picture3;
                    pictureBox12.Image = null;
                    pictureBox12.AllowDrop = true;
                    break;
                case 1:
                    pictureBox4.Image = pictures.picture3;
                    pictureBox5.Image = pictures.picture2;
                    pictureBox6.Image = pictures.picture1;
                    pictureBox7.Image = pictures.picture2;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture1;
                    pictureBox10.Image = pictures.picture3;
                    pictureBox11.Image = pictures.picture2;
                    pictureBox12.Image = null;
                    pictureBox12.AllowDrop = true;
                    break;
                case 2:
                    pictureBox4.Image = null;
                    pictureBox4.AllowDrop = true;
                    pictureBox5.Image = pictures.picture3;
                    pictureBox6.Image = pictures.picture1;
                    pictureBox7.Image = pictures.picture2;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture1;
                    pictureBox10.Image = pictures.picture3;
                    pictureBox11.Image = pictures.picture2;
                    pictureBox12.Image = pictures.picture1;
                    break;
                case 3:
                    pictureBox4.Image = pictures.picture3;
                    pictureBox5.Image = pictures.picture3;
                    pictureBox6.Image = null;
                    pictureBox6.AllowDrop = true;
                    pictureBox7.Image = pictures.picture2;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture2;
                    pictureBox10.Image = pictures.picture1;
                    pictureBox11.Image = pictures.picture1;
                    pictureBox12.Image = pictures.picture1;
                    break;
                case 4:
                    pictureBox4.Image = pictures.picture3;
                    pictureBox6.Image = null;
                    pictureBox6.AllowDrop = true;
                    pictureBox5.Image = pictures.picture2;
                    pictureBox7.Image = pictures.picture2;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture1;
                    pictureBox10.Image = pictures.picture1;
                    pictureBox11.Image = pictures.picture1;
                    pictureBox12.Image = pictures.picture2;
                    break;
                case 5:
                    pictureBox4.Image = pictures.picture3;
                    pictureBox6.Image = pictures.picture3;
                    pictureBox5.Image = pictures.picture2;
                    pictureBox7.Image = null;
                    pictureBox7.AllowDrop = true;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture1;
                    pictureBox10.Image = pictures.picture1;
                    pictureBox11.Image = pictures.picture1;
                    pictureBox12.Image = pictures.picture2;
                    break;
                case 6:
                    pictureBox4.Image = pictures.picture3;
                    pictureBox6.Image = pictures.picture1;
                    pictureBox5.Image = pictures.picture2;
                    pictureBox7.Image = null;
                    pictureBox7.AllowDrop = true;
                    pictureBox8.Image = null;
                    pictureBox8.AllowDrop = true;
                    pictureBox9.Image = pictures.picture2;
                    pictureBox10.Image = pictures.picture3;
                    pictureBox11.Image = pictures.picture1;
                    pictureBox12.Image = pictures.picture2;
                    break;
            }
        }
        public Form1()
        {
            InitializeComponent();
            Random rnd = new Random();
            method = Utils.random_chislo();
            new_gane();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            method = Utils.random_chislo();
            new_gane();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            switch (method)
            {
                case 0:
                    if (pictureBox5.Tag is 3 && pictureBox12.Tag is 1)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 1:
                    if (pictureBox8.Tag is 3 && pictureBox12.Tag is 1)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 2:
                    if (pictureBox4.Tag is 2 && pictureBox8.Tag is 3)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 3:
                    if (pictureBox6.Tag is 3 && pictureBox8.Tag is 2)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 4:
                    if (pictureBox6.Tag is 3 && pictureBox8.Tag is 2)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 5:
                    if(pictureBox7.Tag is 1 && pictureBox8.Tag is  1)
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case 6:
                    if (pictureBox7.Tag is 3 && pictureBox8.Tag is 1)
                    {
                        DialogResult game_over = MessageBox.Show("Вы победили!)", "Победа", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        DialogResult game_over = MessageBox.Show("Вы проиграли:c", "Проигрыш", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    }
                    break;
            }
        }

        private void PictureBox4_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox4_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox4.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox4.Tag = 1;
                    break;
                case 2:
                    pictureBox4.Tag = 2;
                    break;
                case 3:
                    pictureBox4.Tag = 3;
                    break;
            }
        }
       

        private void PictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            answer = 1;
            pictureBox1.DoDragDrop(pictureBox1.Image, DragDropEffects.Copy);
        }

        private void PictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            answer = 2;
            pictureBox2.DoDragDrop(pictureBox2.Image, DragDropEffects.Copy);
        }

        private void PictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            answer = 3;
            pictureBox3.DoDragDrop(pictureBox3.Image, DragDropEffects.Copy);
        }

        private void PictureBox5_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox5_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox5.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox5.Tag = 1;
                    break;
                case 2:
                    pictureBox5.Tag = 2;
                    break;
                case 3:
                    pictureBox5.Tag = 3;
                    break;
            }
        }

        private void PictureBox7_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox7.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox7.Tag = 1;
                    break;
                case 2:
                    pictureBox7.Tag = 2;
                    break;
                case 3:
                    pictureBox7.Tag = 3;
                    break;
            }
        }

        private void PictureBox7_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox8_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox8_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox8.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox8.Tag = 1;
                    break;
                case 2:
                    pictureBox8.Tag = 2;
                    break;
                case 3:
                    pictureBox8.Tag = 3;
                    break;
            }
        }

        private void PictureBox9_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox9.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox9.Tag = 1;
                    break;
                case 2:
                    pictureBox9.Tag = 2;
                    break;
                case 3:
                    pictureBox9.Tag = 3;
                    break;
            }
        }

        private void PictureBox9_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox10_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox10_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox10.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox10.Tag = 1;
                    break;
                case 2:
                    pictureBox10.Tag = 2;
                    break;
                case 3:
                    pictureBox10.Tag = 3;
                    break;
            }
        }

        private void PictureBox11_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox11_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox11.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox11.Tag = 1;
                    break;
                case 2:
                    pictureBox11.Tag = 2;
                    break;
                case 3:
                    pictureBox11.Tag = 3;
                    break;
            }
        }

        private void PictureBox12_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox12_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox12.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox12.Tag = 1;
                    break;
                case 2:
                    pictureBox12.Tag = 2;
                    break;
                case 3:
                    pictureBox12.Tag = 3;
                    break;
            }
        }


        private void PictureBox6_DragEnter_1(object sender, DragEventArgs e)
        {
            e.Effect = e.AllowedEffect;
        }

        private void PictureBox6_DragDrop(object sender, DragEventArgs e)
        {
            pictureBox6.Image = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            switch (answer)
            {
                case 1:
                    pictureBox6.Tag = 1;
                    break;
                case 2:
                    pictureBox6.Tag = 2;
                    break;
                case 3:
                    pictureBox6.Tag = 3;
                    break;
            }
        }
    }
}
